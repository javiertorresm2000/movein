<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<script src="../js/jquery-3.4.1.min.js"></script>
    
	<title>Move-in</title>
</head>
<body>
	<div class="general2" >
		<header class="encabezado">
			<h1 class="logo">MOVE-IN</h1>
			<div class="menu">
				<div>CONFIGURACIÓN</div>
				<div id="user">
					<div><a href="index.php">CERRAR SESIÓN</a></div>
				</div>
			</div>
		</header>
		<div class="cuerpo">
			
			<div class="central2" id="central2">
				<div class="buscar"><input type="text" placeholder="Buscar"></div>
				<div class="filtros">
					<div class="filtro">Precio</div>
					<div class="filtro">Huespédes</div>
					<div class="filtro">Más filtros</div>
				</div>
				<div id="alojamientos" class="alojamientos"></div>
			</div>
			<!-- INICIO VISTA DETALLES DEL ALOJAMIENTO-->
			<div class="detalles" id="detallesAlojamiento" style="display: none;"></div>		
		</div>	
	</div>
</body>
<script src="../js/funciones_principal_huesped.js"></script>
<script src="../js/peticiones_principal_huesped.js"></script>

</html>