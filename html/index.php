<?php
	if(!isset($_SESSION)){
		session_start();
	}
	else{
		session_destroy();
		session_start();
	}
	
	//error_reporting(1);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<script src="../js/jquery-3.4.1.min.js"></script>
	<title>Move-in Huesped</title>
</head>
<body>
	<div class="general">
		<header class="encabezado">
			<h1 class="logo">MOVE-IN</h1>
			<div style="width:40%; float:right; margin-right:30px;"> 
				
				<div><a href="sesion_anfitrion.php" style="text-decoration:none !important; outline:none !important; color:white; ">Quiero ser Anfitrión!</a></div>
			</div>
			
		</header>
		<div class="modal">
			<div class="sesion"><h2>INICIAR SESIÓN</h2></div>
			<div id="redes">
				<button class="boton">
					<img class="imagen" src="google.png">
					<h3>Google</h3>
				</button>
				<button id="facebook">
					<img class="imagen" src="facebook.png">
					<h3>Facebook</h3>
				</button>
			</div>
			<img class="o" src="o.png">
			<div class="login">
				<input id="email" type="mail" placeholder="Email">
				<input id="contrasenia" type="password" placeholder="Contraseña">
				<button id="btn_iniciar_sesion_huesped" class="boton">INICIAR SESIÓN</button>
				<h5  id="recuperar"><a href="recuperar.html">Recuperar mi contraseña</a></h5>
				<h5>¿No tienes cuenta? <a id="registro" href="registro_huesped.php">Regístrate aquí</a></h5>
			</div>
		</div>
	</div>
</body>
<script src="../js/funciones_sesion_huesped.js"></script>
<script src="../js/peticiones_sesion_huesped.js"></script>
</html>