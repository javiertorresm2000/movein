
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<script src="../js/jquery-3.4.1.min.js"></script>
    
	<title>Move-in</title>
</head>
    <?php

        if(!isset ($_SESSION)){
            session_start();
        }
    ?>
<body>
    <script>
        /*VARIABLES GLOBALES*/
        var CorreoAnfitrion = '<?php echo $_SESSION['Correo'];?>';
        var IdAnfitrion  = '<?php echo $_SESSION['IdAnfitrion'];?>';
        
    </script>
	<div class="general2" >
		<header class="encabezado">
			<h1 class="logo">MOVE-IN</h1>
			<div class="menu" style="float:right !important;">
                    <div id="user" style="float:right; !important">
                        <div id="img_user" style="margin: 0px 20px 0px 50px;"></div>
                        <div><a href="sesion_anfitrion.php" style="text-decoration:none !important; outline:none !important; color:white; ">CERRAR SESIÓN</a></div>
                    </div>
				
			</div>
		</header>
		<div class="cuerpo">
			
			<div class="central2" id="central2">
                <div style="height: 38px; float:left;">
                    <button class="btn_success" id="btn_agregar_alojamiento"> + | Agregar </button>
                </div>
                <br>
                <div style="height: auto; float:left; margin-left:100px;">
                    <h2 style=>Mis Alojamientos</h2>
                </div>

				<div style="margin-top: 30px;" id="alojamientos" class="alojamientos">
                    
				</div>
				<!--<img src="mapa.png">-->
            </div>
            <div id="detallesAlojamiento" style="display:none;"></div>

            <div class="agregarAloj" id="agregarAloj" style="display:none; overflow-y:scroll !important;">
                
                <br>
                <div style="height: auto; float:left; margin-left:40%; margin-top:-10px !important;">
                    <h1 style=>Nuevo Alojamiento</h1>
                </div>
                <br>
                <br>
                <h3>Informacion</h3>
                <br>
                <div class="divs_inputs" style="margin-right:2%">
                    <label for="">Nombre Corto</label>
                    <input type="text" id="NombreCorto" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Direccion</label>
                <input type="text" id="Direccion" class="inputs_aloj">
                </div>
                <div class="divs_inputs">
                <label for="">Colonia</label>
                <input type="text" id="Colonia" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Municipio</label>
                <input type="text" id="Municipio" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Estado</label>
                <input type="text" id="Estado" class="inputs_aloj">
                </div>
                <div class="divs_inputs">
                <label for="">Codigo Postal</label>
                <input type="text" id="CodigoPostal" class="inputs_aloj">
                </div>
                <div class="divs_inputs">
                <label for="">Descripción</label>
                <input type="text" id="Descripcion" placeholder="Describa su alojamiento en menos de 200 palabras" class="inputs_aloj" style="width:100% !important; height: 70px !important;">
                </div>


                <h3 style="width:100%; height: auto; float: left; margin-top:20px; margin-bottom:10px;">Detalles Generales</h3>

                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Numero de Pisos</label>
                <input type="number" id="NumPisos" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Numero Cuartos Completos</label>
                <input type="number" id="NumCuartosCompletos" class="inputs_aloj">
                </div>
                <div class="divs_inputs">
                <label for="">Numero Cuartos Compartidos</label>
                <input type="number" id="NumCuartosCompartidos" class="inputs_aloj" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Numerp BañosCompletos</label>
                <input type="number" id="NumBaniosCompletos" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Numero Baños Medios</label>
                <input type="number" id="NumBaniosMedios" class="inputs_aloj">
                </div>
                <div class="divs_inputs" >
                <label for="">Numero de Cocinas</label>
                <input type="number" id="NumCocinas" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Numero de Areas de Lavado</label>
                <input type="number" id="NumLavado" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Numer de Comedores</label>
                <input type="number" id="NumComedor" class="inputs_aloj">
                </div>
                <div class="divs_inputs">
                <label for="">Numerp Salas de Estar</label>
                <input type="number" id="NumSalaDeEstar" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">Numero de Patios</label>
                <input type="number" id="NumPatios" class="inputs_aloj">
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                <label for="">NumAutosCochera</label>
                <input type="number" id="NumAutosCochera" class="inputs_aloj">
                </div>
                

                <h3 style="width:100%; height: auto; float: left; margin-top:20px; margin-bottom:10px;">
                Servicios Incluidos</h3>
                <div class="divs_inputs" style="margin-right:2%">
                    <label for="">Agua</label>
                    <select name="" id="ServicioAgua" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                    <lable> Drenaje
                    <select name="" id="ServicioDrenaje" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>
                <div class="divs_inputs">
                    <lable> Electricidad
                    <select name="" id="ServicioElectricidad" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                    <label> Gas
                    <select name="" id="ServicioGas" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>

                <div class="divs_inputs" style="margin-right:2%">
                    <label> Calentador
                    <select name="" id="ServicioCalentador" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>
                <div class="divs_inputs">
                    <label> Telefono
                    <select name="" id="ServicioTelefono" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>

                <div class="divs_inputs" style="margin-right:2%">
                    <label"> Internet
                    <select name="" id="ServicioInternet" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0" >NO</option>
                    </select>
                </div>

                <div class="divs_inputs" style="margin-right:2%">
                    <label> TvCable
                    <select name="" id="ServicioTvCable" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0">NO</option>
                    </select>
                </div>
                <div class="divs_inputs">
                    <label> Limpieza
                    <select name="" id="ServicioLimpieza" class="inputs_aloj">
                        <option value="1" selected>SI</option>
                        <option value="0" selected>NO</option>
                    </select>
                </div>
                
                <h3 style="width:100%; height: auto; float: left; margin-top:20px; margin-bottom:10px;">
                Especificaciones de Renta</h3>
                <div class="divs_inputs" style="margin-right:2%">
                    <label for="">Modalidad </label>
                    <select name="" id="ModalidadRenta" class="inputs_aloj">
                        <option value="1" selected>Alojamiento Completo</option>
                        <option value="2">Habitación</option>
                    </select>
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                    <label for="">Huespedes</label>
                    <input type="number" id="Huespedes" class="inputs_aloj">
                </div>
                <div class="divs_inputs">
                    <label for="">Estancia minima (Meses)</label>
                    <select name="" id="EstanciaMinima" class="inputs_aloj">
                        <option value="1" selected>1 Meses</option>
                        <option value="2" selected>2 Meses</option>
                        <option value="3" selected>3 Meses</option>
                        <option value="4" selected>4 Meses</option>
                        <option value="5" selected>5 Meses</option>
                        <option value="6" selected>6 Meses</option>
                        <option value="7" selected>7 Meses</option>
                        <option value="8" selected>8 Meses</option>
                        <option value="9" selected>9 Meses</option>
                        <option value="10" selected>10 Meses</option>
                        <option value="11" selected>11 Meses</option>
                        <option value="12" selected>12 Meses</option>
                    </select>
                </div>
                <div class="divs_inputs" style="margin-right:2%">
                    <label for="">Costo Renta Mensual</label>
                    <input type="number" step="any" min="0" id="CostoRenta" class="inputs_aloj"/>
                </div>
                <br>
                <div style= "width:100%; float:left;">
                    <h3 style="width:100%; height: auto; float: left; margin-top:20px; margin-bottom:10px;">Sube una foto</h3>
                    <form enctype="multipart/form-data" name="subirFoto" id="subirFoto">
                        
                        <input type="file" id="file" name="file" style="border: 0px; width : 100%;"/>
                        <input type="submit" value="Agregar Foto" id="upload"class="btn_success" style="background-color: #0CD;"/>
                    </form>
                </div>
                <div style= "width:100%; float:left; border-top: 1px solid #ccc; margin-top: 20px;padding-top:20px" >
                    <div style="height: 38px; float:right; width:32%; margin-left:2%">
                        <button class="btn_success" id="btn_guardar_alojamiento" style="width:100% !important;">Guardar Alojamiento</button>
                    </div>
                    <div style="height: 38px; float:right; width:32%" >
                        <button class="btn_danger" id="btn_cancelar_alojamiento" style="width:100% !important;"> Cancelar </button>
                    </div>
                </div>
            </div>
		</div>
	</div>
</body>
<script src="../js/funciones_principal_anfitrion.js"></script>
<script src="../js/peticiones_principal_anfitrion.js"></script>
</html>