
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<script src="../js/jquery-3.4.1.min.js"></script>
	<title>Move-in</title>
</head>
<body>
	<div class="general">
		<header class="encabezado">
			<h1 class="logo">MOVE-IN ANFITRIÓN</h1>
			<div style="width:40%; float:right; margin-right:30px;"> 
				
				<div><a href="index.php" style="text-decoration:none !important; outline:none !important; color:white; ">Sesión Huesped</a></div>
			</div>
			
		</header>
		<div class="modal">
			<div class="sesion"><h2>INICIAR SESIÓN</h2></div>
			<div id="redes">
				<button class="boton">
					<img class="imagen" src="google.png">
					<h3>Google</h3>
				</button>
				<button id="facebook">
					<img class="imagen" src="facebook.png">
					<h3>Facebook</h3>
				</button>
			</div>
			<img class="o" src="o.png">
			<div class="login">
				<input id="email_anfitrion" type="mail" placeholder="Email">
				<input id="contrasenia_anfitrion" type="password" placeholder="Contraseña">
				<button id="btn_iniciar_sesion_anfitrion" class="boton">INICIAR SESIÓN</button>
				<h5  id="recuperar"><a href="recuperar.html">Recuperar mi contraseña</a></h5>
				<h5>¿No tienes cuenta? <a id="registro" href="registro_anfitrion.php">Regístrate aquí</a></h5>
			</div>
		</div>
	</div>
</body>
<script src="../js/funciones_sesion_anfitrion.js"></script>
<script src="../js/peticiones_sesion_anfitrion.js"></script>
</html>