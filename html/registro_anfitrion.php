<!DOCTYPE html>
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../css/estilos.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<script src="../js/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>

<body>
    <div class="general">
		<header class="encabezado">
            <h1 class="logo">MOVE-IN ANFITRION</h1>
            <div style="width:40%: float:left">
                <div style="padding-right:50px; width: 60%; float: right"><span id="link_sesion_anfitrion" >Iniciar Sesión</span></div> 
                
            </div>
		</header>
		<div class="vista_izquierda">
            <img src="../img/mapa.PNG" alt="">
        </div>
        <div class="vista_derecha" style="overflow-y:scroll;">
            <h2>Registrate</h2>
            <hr style="width:40px; height:10px; border-radius:4px; border:0px; background-color:#0dc; margin-top:10px; margin-bottom:0px;">
            <br>
            <div class="divs_registro">
                <span>Correo</span>
                <input type="email" name="correo" id="correo_anfitrion" class="inputs" required>
            </div>
            <div class="divs_registro">
                <span>Contraseña</span>
                <input type="password" name="contrasenia" id="contrasenia_anfitrion" class="inputs" required>
            </div>
            <div class="divs_registro">
                <span>Confirmar Contraseña</span>
                <input type="password" name="conf_contrasenia" id="conf_contrasenia_anfitrion" class="inputs" required>
            </div>
            <div class="divs_registro">
                <span>Nombre</span>
                <input type="text" name="nombre" id="nombre_anfitrion" class="inputs" required>
            </div>
            <div class="divs_registro">
                <span>Apellido(s)</span>
                <input type="text" name="apellido" id="apellido_anfitrion" class="inputs" required>
            </div>
            <div class="divs_registro" style="width: 45% !important; float: left;">
                <span>Sexo</span>
                <select name="sexo" id="sexo_anfitrion" class="inputs">
                    <option value="H">Hombre</option>
                    <option value="M">Mujer</option>
                </select>
            </div>
            <div class="divs_registro" style="margin-left: 10%;width: 45% !important; float: right;">
                <span>Fecha Nacimiento</span>
                <input type="date" name="fechaNac" id="fechaNac_anfitrion" class="inputs" required>
            </div>
            <div class="divs_registro" style="width: 45% !important; float: left;">
                <span>Telefono</span>
                <input type="text" name="pais" id="telefono_anfitrion" class="inputs" required >
            </div>
            <div class="divs_registro" style="margin-left: 10%;width: 45% !important; float: right;">
                <span>Idioma</span>
                <select name="idioma" id="idioma_anfitrion" class="inputs">
                    <option value="Espaniol" selected>Español</option>
                    <option value="Ingles">Ingles</option>
                    <option value="Frances">Frances</option>
                    <option value="Aleman">Aleman</option>
                    <option value="Portugues">Portugues</option>
                    <option value="Mandarin">Mandarin</option>
                </select>
            </div>
            <div class="divs_registro">
                <span>Direccion</span>
                <input type="text" id="direccion_anfitrion" class="inputs" required>
            </div>
            <div class="divs_registro" style="width: 45% !important; float: left;">
                <span>Ciudad</span>
                <input type="text"  id="ciudad_anfitrion" class="inputs" required>
            </div>
            <div class="divs_registro" style="margin-left: 10%;width: 45% !important; float: right;">
                <span>Estado</span>
                <input type="text" id="estado_anfitrion" class="inputs"required >
            </div>
            <div class="divs_registro">  
            <span>Sube una foto</span>
                <form enctype="multipart/form-data" name="subirFoto" id="subirFoto">
                    
                    <input type="file" id="file" name="file" style="border: 0px; width : 100%;"/>
                    <input type="submit" value="Agregar Foto" id="upload"class="btn_success" style="background-color: #0CD;"/>
                </form>
            </div>
            <br>
            <div class="divs_registro">
                <button class="btn_success" id="btn_registro_anfitrion" style="float: right;">Registrarme!</button>
                <button class="btn_danger" id="btn_cancelar_registro_anfitrion" style="float: right; margin-right:20px;">Cancelar</button>
                <br><br>
            </div>
            <br>
        </div>
	</div>
</body>
<script src="../js/funciones_registro_anfitrion.js"></script>
<script src="../js/peticiones_registro_anfitrion.js"></script>
</html>