var protocol = 'http://';
var host = '192.168.64.2';
var foto="";


function registro_huesped(){
    let datos_huesped={
        Correo: $('#correo_huesped').val(),
        Contrasenia: $('#contrasenia_huesped').val(),  
        Nombre : $('#nombre_huesped').val(),
        Apellido : $('#apellido_huesped').val(),
        Sexo: $('#sexo_huesped').val(),
        Nacionalidad: $('#nacionalidad_huesped').val(),
        Idioma: $('#idioma_huesped').val(),
        FechaNac: $('#fechaNac_huesped').val(),
        Foto: foto
    };

    if(datos_huesped.Correo=="" || datos_huesped.Contrasenia=="" || datos_huesped.Nombre=="" || datos_anfitrion.Apellido=="" ||
    datos_huesped.Sexo=="" || datos_huesped.FechaNac=="" || datos_huesped.Nacionalidad=="" || 
     datos_anfitrion.Foto==""){
        alert("Necesita llenar todos los campos para poderse registrar! :)")
    }
    else{
        $.ajax({
            url : protocol+host+'/Movein/api/huespedes/?action=insert_huesped',
            type: 'POST',
            dataType: 'JSON',
            data: {
                datos_huesped : datos_huesped
            },
            beforeSend: function(){

            },
            success: function(response){
                if(response.status == 'ok'){
                    alert("Usuario registrado con Éxito!")
                    window.location='index.php';
                }
                else{
                    alert(response.body);
                }
            }
        });
    }
}


//var formulario = $('#subirFoto'); //Donde el el selector subirFoto debe estar dentro de la etiqueta form como id es decir <form id="subirFoto" enctype="multipart/form-data">
/*
formulario = document.getElementById('subirFoto');
formulario.submit(function( event ) {
event.preventDefault();
$data = $formulario.serialize();

    $.ajax({
        
        type: 'POST',
        url: protocol+host+'/Movein/api/saveImg',//Ruta donde va a ser enviado el formulario
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            //que quieres hacer antes de enviar la información
        },
        success: function (response) {

               //Response devuelve una respuesta del archivo subirFoto.php
               //Puede ser un json que es lo mas común utilizar en estos casos
        },
        error: function (response) {
               //En caso de que suceda algún error con el envío de información
        }
    });
});
*/

$(document).ready(function(){
    $("#subirFoto").on('submit', function(e){
        e.preventDefault();
        foto="";
        //var blobFile = document.getElementById('img').files[0];
        // formData = new FormData();
        //formData.append("img_up", blobFile);
       // formData = new FormData(document.getElementById ("subirFoto"));
        //var f = $(this);
        //console.log(blobFile);
        //var formData = new FormData();    
        //formData.append( 'img', blobFile);
        //var blobFile = $('#img_up').files[0];
        //var formData = new FormData(document.getElementById("subirFoto"));
        //formData.append(f.attr("name"), blobFile);
        //formData.append('img_up', blobFile);
        //console.log(formData);
        $.ajax({
            type: "POST",
            url: protocol+host+'/Movein/api/saveImg/index.php',
            data: new FormData(document.getElementById("subirFoto")),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function() { 
            },
            success: function(response){
                // AQUI SE HACE TRIM() PARA QUITARLE LAS COMILLAS CON LAS QUE VIENE EL NOMBRE DE LA FOTO
                // Y PODER GUARDAR CORRECTAMENTE LA RUTA
                var array = response.trim("");
                for(var i=1;i<array.length-1;i++){
                    foto+=""+array[i];
                }
                alert("La imagen: "+foto+ " ha sido cargada con exito");
            }
        })
    })
});

/*
var form = document.getElementById("subirFoto");
form.addEventListener('submit', function(ev) {

    var x = document.getElementById('img').files[0];
      oData = new FormData();

  oData.append("CustomField", x);

  var oReq = new XMLHttpRequest();
  oReq.open("POST", protocol+host+'/Movein/api/saveImg/index.php', true);
  oReq.onload = function(ev) {
    if (oReq.status == 200) {
      alert("Llego");
    } else {
      alert("No Llego");
    }
  };
  oReq.send(oData);
  ev.preventDefault();
}, false);*/


