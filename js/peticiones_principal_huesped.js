
var protocol = 'http://';
var host = '192.168.64.2';

function listarAlojamientos(){
    $.ajax({
        url : protocol+host+'/Movein/api/alojamientos/?action=select_alojamiento',
        type: 'GET',
        dataType: 'JSON',
        beforeSend: function(){

        },
        success: function(response){
            if(response.status == 'ok'){
                peticionListarAlojamientos(response.body);/*
                console.log(response);
                for(var i=0; i<response.body.length;i++){
                    $('#alojamientos').append(
                        '<div class="alojamiento" id="'+response.body[i].IdAlojamiento+'">'+
                            '<div class="imgAlojamiento">'+
                                '<img src="'+response.body[i].Foto+'">'+
                            '</div>'+
                            '<div class="infoAlojamiento">'+
                                '<div class="nombreAlojamiento">'+
                                    '<span>'+response.body[i].NombreCorto+'</span>'+
                                '</div>'+
                                '<div class="detallesAlojamiento">'+
                                    '<span style="margin-right:15px;">'+response.body[i].NumCuartosCompletos+' Cuartos Completos </span>'+
                                    '<span style="margin-right:15px;">'+response.body[i].NumBaniosCompletos+' Baños </span>'+
                                    '<span>'+response.body[i].Huespedes+' Huespedes </span>'+
                                '</div>'+
                                '<button class="btn_success" style="float:right;" id="'+response.body[i].IdAlojamiento+'">Información</button>'+
                       
                            '</div>'+
                             '</div>'
                    );
                }*/
            }
            else{
                alert("No hay alojamientos disponibles en este momento");
            }
        }
    });
}

function peticionListarAlojamientos(datos){
    var renta="";
    for(var i=0; i<datos.length;i++){
    if(datos[i].ModalidadRenta==1){
        renta="Alojamiento Completo";
    }
    else{
        renta="Habitación"
    }
        $('#alojamientos').append(
            '<div class="alojamiento" id="'+datos[i].IdAlojamiento+'" onclick="getIdAlojamiento(this.id)">'+
                '<div class="imgAlojamiento">'+
                    '<img src="img/'+datos[i].Foto+'" style="width:100%; height: 100%;">'+
                '</div>'+
                '<div class="infoAlojamiento">'+
                    '<div class="nombreAlojamiento">'+
                        '<span>'+datos[i].NombreCorto+'</span>'+
                    '</div>'+
                    '<div class="detallesAlojamiento">'+
                        '<span style="margin-right:15px;">'+datos[i].NumCuartosCompletos+' Cuartos Completos </span>'+
                        '<span style="margin-right:15px;">'+datos[i].NumBaniosCompletos+' Baños </span>'+
                        '<span>'+datos[i].Huespedes+' Huespedes </span>'+
                    '</div>'+
                    '<div style="margin-top:30px;">'+
                        '<span style="font-size: 16px;"> Modalidad: '+renta+
                    '</div>'+
                    '<div style="margin-top:30px;">'+
                    '<span style="font-size: 18px;"> Costo de la renta: $'+datos[i].CostoRenta+
                    '</div>'+
           
                '</div>'+
            '</div>'
        );
    }
    
}

function detallesAlojamiento(IdAlojamiento){
    document.getElementById('central2').style.display="none";
    document.getElementById('detallesAlojamiento').style.display="block";

    $.ajax({
        url : protocol+host+'/Movein/api/alojamientos/?action=select_alojamiento_especifico',
        type: 'GET',
        dataType: 'JSON',
        data:{
            IdAlojamiento : IdAlojamiento
        },
        beforeSend: function(){

        },
        success: function(response){
            if(response.status == 'ok'){
                peticionMostrarDetallesAlojamiento(response.body);
            }
            else{

            }
        }
    });
}

function peticionMostrarDetallesAlojamiento(alojamiento){
    $("#detallesAlojamiento").empty();
    //Validaciones Servicios
    var agua,luz,drenaje,internet,telefono,calentador,gas,tvCable, limpieza;
    if(alojamiento.ServicioAgua==1){agua="SI";}
    else{agua="NO"}

    if(alojamiento.ServicioElectricidad==1){luz="SI";}
    else{luz="NO"}

    if(alojamiento.ServicioDrenaje==1){drenaje="SI";}
    else{drenaje="NO"}

    if(alojamiento.ServicioInternet==1){internet="SI";}
    else{internet="NO"}

    if(alojamiento.ServicioTelefono==1){telefono="SI";}
    else{telefono="NO"}

    if(alojamiento.ServicioCalentador==1){calentador="SI";}
    else{calentador="NO"}

    if(alojamiento.ServicioGas==1){gas="SI";}
    else{gas="NO"}

    if(alojamiento.ServicioTvCable==1){tvCable="SI";}
    else{tvCable="NO"}

    if(alojamiento.ServicioLimpieza==1){limpieza="SI";}
    else{limpieza="NO"}


    var img="img/"+alojamiento.Foto;
    //for(var i=0; i<datos.length;i++){
        $('#detallesAlojamiento').append(
            '<div class="left-panel" id="'+alojamiento.IdAlojamiento+'">'+
                '<div><h2>'+alojamiento.NombreCorto+'</h2></div>'+
                '<div class="detallesAlojamiento">'+
                    '<span style="margin-right:15px;">'+alojamiento.NumCuartosCompletos+' Cuartos Completos </span>'+
                    '<span style="margin-right:15px;">'+alojamiento.NumBaniosCompletos+' Baños </span>'+
                    '<span>'+alojamiento.Huespedes+' Huespedes </span>'+
                '</div>'+
                '<div style="width:100%; margin: 15px 0px">'+alojamiento.Descripcion+'</div>'+
                '<h3> Ubicacion </h3>'+
                '<div style="width:100%; margin-bottom 10px;">'+alojamiento.Direccion+'</div>'+
                '<h3> Características </h3>'+
                '<div class="left-panel">'+
                    '<div style="width:100%; margin-bottom 10px;"> Cantidad Huespedes: '+alojamiento.Huespedes+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> No. Pisos: '+alojamiento.NumPisos+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Cuartos completos: '+alojamiento.NumCuartosCompletos+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Cuartos compartidos: '+alojamiento.NumCuartosCompletos+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Baños Completos: '+alojamiento.NumBaniosCompletos+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Baños Compartidos: '+alojamiento.NumBaniosMedios+'</div>'+
                '</div>'+
                '<div class="right-panel">'+
                    '<div style="width:100%; margin-bottom 10px;"> No. Cocinas: '+alojamiento.NumCocinas+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> No. Area de Lavado: '+alojamiento.NumLavado+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> No. Salas de Estar: '+alojamiento.NumSalaDeEstar+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> No. Comedores: '+alojamiento.NumComedor+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> No. Patios: '+alojamiento.NumPatios+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> No. Cocheras: '+alojamiento.NumAutosCochera+'</div>'+
                '</div>'+
                '<h3> Servicios Incluidos </h3>'+
                '<div class="left-panel">'+
                    '<div style="width:100%; margin-bottom 10px;"> Agua:'+agua+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Electricidad:'+luz+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Gas: '+gas+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Drenaje: '+drenaje+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Calentador: '+calentador+'</div>'+
                '</div>'+
                '<div class="right-panel">'+
                    '<div style="width:100%; margin-bottom 10px;"> Internet: '+internet+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Teléfono: '+telefono+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> TV por Cable:  '+tvCable+'</div>'+
                    '<div style="width:100%; margin-bottom 10px;"> Limpieza: '+limpieza+'</div>'+
                '</div>'+
                '<div>'+
                    '<button class="btn_danger" onclick="regresar()">Regresar</button>'+
                '</div>'+
            '</div>'+
            '<div class="right-panel">'+
                '<div class="imgAlojSelec">'+
                    '<img src="'+img+'" style="width:100%; height: 100%;">'+
                '</div>'+
            '</div>'
        );
    //}
}