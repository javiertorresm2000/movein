var protocol = 'http://';
var host = '192.168.64.2';
var foto="";

function registro_anfitrion(){
    let datos_anfitrion={
        Correo: $('#correo_anfitrion').val(),
        Contrasenia: $('#contrasenia_anfitrion').val(),  
        Nombre : $('#nombre_anfitrion').val(),
        Apellido : $('#apellido_anfitrion').val(),
        Sexo: $('#sexo_anfitrion').val(),
        Idioma: $('#idioma_anfitrion').val(),
        FechaNac: $('#fechaNac_anfitrion').val(),
        Direccion: $('#direccion_anfitrion').val(),
        Ciudad: $('#ciudad_anfitrion').val(),
        Estado: $('#estado_anfitrion').val(),
        Telefono: $('#telefono_anfitrion').val(),
        Foto: foto
    };

    if(datos_anfitrion.Correo=="" || datos_anfitrion.Contrasenia=="" || datos_anfitrion.Nombre=="" || datos_anfitrion.Apellido=="" ||
    datos_anfitrion.Sexo=="" || datos_anfitrion.FechaNac=="" || datos_anfitrion.Ciudad=="" || datos_anfitrion.Estado=="" || datos_anfitrion.Telefono==""
    || datos_anfitrion.Foto==""){
        alert("Necesita llenar todos los campos para poderse registrar! :)")
    }
    else{
        $.ajax({
            url : protocol+host+'/Movein/api/anfitriones/?action=insert_anfitrion',
            type: 'POST',
            dataType: 'JSON',
            data: {
                datos_anfitrion : datos_anfitrion
            },
            beforeSend: function(){
    
            },
            success: function(response){
                if(response.status == 'ok'){
                    alert("Usuario registrado con Éxito!")
                    window.location='sesion_anfitrion.php';
                }
                else{
                    alert(response.body);
                }
            }
        });
    }
}


$(document).ready(function(){
    $("#subirFoto").on('submit', function(e){
        e.preventDefault();
        foto="";
        $.ajax({
            type: "POST",
            url: protocol+host+'/Movein/api/saveImg/index.php',
            data: new FormData(document.getElementById("subirFoto")),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function() { 
            },
            success: function(response){
                // AQUI SE HACE TRIM() PARA QUITARLE LAS COMILLAS CON LAS QUE VIENE EL NOMBRE DE LA FOTO
                // Y PODER GUARDARCORRECTAMENTE LA RUTA
                var array = response.trim("");
                for(var i=1;i<array.length-1;i++){
                    foto+=""+array[i];
                }
                alert("La imagen: "+foto+ " ha sido cargada con exito");
            }
        })
    })
});