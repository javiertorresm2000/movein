
$(document).ready(function(){	
    
    getFotoUsuario(IdAnfitrion);
    //CARGA TODOS LOS ALOJAMIENTOS QUE ESE USUARIO TIENE EN RENTA
	$("#alojamientos").empty();
    listarAlojamientosAnfitrion(IdAnfitrion);
    console.log(IdAnfitrion);
    console.log(CorreoAnfitrion)
    
});

// GUARDA UN NUEVO ALOJAMIENTO
document.getElementById('btn_guardar_alojamiento').addEventListener('click', function(e){
    insertAlojamiento(IdAnfitrion);
});

//Cambia vista para insertar alojamiento
document.getElementById("btn_agregar_alojamiento").addEventListener('click', function(){

    document.getElementById('central2').style.display="none";
    document.getElementById('agregarAloj').style.display="block";
});

//Regresa la vista principal del anfitrion
document.getElementById("btn_cancelar_alojamiento").addEventListener('click', function(){
    document.getElementById('central2').style.display="block";
    document.getElementById('agregarAloj').style.display="none";
});

//Obtiene el ID del alojamiento incrustado en el div contenedor
//Para poder hacer la consulta con el server y traer los detalles de ese alojamiento
function getIdAlojamiento(IdAlojamiento){
    detallesAlojamiento(IdAlojamiento);
}
//Regresa de la vista de detalle a la principal
function regresar(){
    document.getElementById('central2').style.display="block";
    document.getElementById('detallesAlojamiento').style.display="none";
}