<?php
  header('Access-Control-Allow-Origin: *');
  ini_set('display_errors',1);
  error_reporting(E_ALL);

  require_once '../models/Huesped.php';
  $class = new Huesped();

  $response = array();
  switch($_GET['action'])
  {
    case 'get_huesped': //Obtener registros de este controlador
    {
      if(isset($_GET['id']))
        $response = $class->get_huespedEspecifico(trim($_GET['id']));
      else
        $response = $class->get_huespedes();
      break;
    }

    case 'insert_huesped':
    {
      $response = $class->insert_huesped($_POST['datos_huesped']);
      break;
    }

    case 'PUT':
    {
      $put_param = array();
      parse_str(file_get_contents('php://input'), $put_param);
  		$GLOBALS['_PUT'] = $put_param;
      
      break;
    }

    case 'DELETE':
    {
      $delete_param = array();
      parse_str(file_get_contents('php://input'), $delete_param);
  		$GLOBALS['_DELETE'] = $delete_param;

      break;
    }

    default:
      header('HTTP/1.1 405 Not Found');
      header('Allow: GET, POST, PUT, DELETE');
      break;
  }
  echo json_encode($response);
 ?>
