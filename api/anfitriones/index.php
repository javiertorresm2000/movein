<?php
  header('Access-Control-Allow-Origin: *');
  ini_set('display_errors',1);
  error_reporting(E_ALL);

  require_once '../models/Anfitrion.php';
  $class = new Anfitrion();

  $response = array();
  switch($_GET['action'])
  {
    case 'select_anfitrion': //Obtener registros de este controlador
    {
        $response = $class->get_anfitrion($_GET['IdAnfitrion']);
      break;
    }

    case 'select_anfitriones':{
      $response = $class->get_anfitriones();
    break;
    }

    case 'insert_anfitrion':
    {
      $response = $class->insert_anfitrion($_POST['datos_anfitrion']);
      break;
    }

    case 'DELETE':
    {

      break;
    }

    default:
      header('HTTP/1.1 405 Not Found');
      header('Allow: GET, POST, PUT, DELETE');
      break;
  }
  echo json_encode($response);
 ?>
