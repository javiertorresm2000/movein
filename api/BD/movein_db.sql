-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 05-12-2019 a las 04:29:46
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `movein_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Alojamiento`
--

CREATE TABLE `Alojamiento` (
  `IdAlojamiento` int(10) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `Colonia` varchar(50) NOT NULL,
  `Municipio` varchar(30) NOT NULL,
  `Estado` varchar(30) NOT NULL,
  `CodigoPostal` varchar(7) NOT NULL,
  `Descripcion` varchar(250) NOT NULL,
  `NumPisos` tinyint(2) DEFAULT NULL,
  `NumCuartosCompletos` tinyint(2) NOT NULL,
  `NumCuartosCompartidos` tinyint(2) DEFAULT NULL,
  `NumBaniosCompletos` tinyint(2) NOT NULL,
  `NumBaniosMedios` tinyint(2) DEFAULT NULL,
  `NumCocinas` tinyint(2) NOT NULL,
  `NumLavado` tinyint(2) NOT NULL,
  `NumComedor` tinyint(2) DEFAULT NULL,
  `NumSalaDeEstar` tinyint(2) DEFAULT NULL,
  `NumPatios` tinyint(2) DEFAULT NULL,
  `NumAutosCochera` tinyint(2) DEFAULT NULL,
  `ServicioAgua` tinyint(1) NOT NULL,
  `ServicioDrenaje` tinyint(1) NOT NULL,
  `ServicioElectricidad` tinyint(1) NOT NULL,
  `ServicioGas` tinyint(1) NOT NULL,
  `ServicioCalentador` tinyint(1) NOT NULL,
  `ServicioTelefono` tinyint(1) NOT NULL,
  `ServicioInternet` tinyint(1) NOT NULL,
  `ServicioTvCable` tinyint(1) NOT NULL,
  `ServicioLimpieza` tinyint(1) NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `FechaRegistro` date NOT NULL,
  `ModalidadRenta` tinyint(1) NOT NULL,
  `Huespedes` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Contratos`
--

CREATE TABLE `Contratos` (
  `IdContrato` int(11) NOT NULL,
  `Anfitrion` int(11) NOT NULL,
  `Huesped` int(11) NOT NULL,
  `Duracion` tinyint(2) NOT NULL,
  `FormaPago` tinyint(1) NOT NULL,
  `ModalidadPago` tinyint(1) NOT NULL,
  `FechaExpedicion` date NOT NULL,
  `FechaVencimiento` date NOT NULL,
  `Monto` decimal(6,2) NOT NULL,
  `ClausulasEstandar` text NOT NULL,
  `FirmaHuesped` varchar(20) NOT NULL,
  `FirmaAnfitrion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Det_Aloj_Muebles`
--

CREATE TABLE `Det_Aloj_Muebles` (
  `IdDetalle` int(11) NOT NULL,
  `Alojamiento` int(11) NOT NULL,
  `Muebles` int(3) NOT NULL,
  `Cantidad` int(2) NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `FechaRegistro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Escuela`
--

CREATE TABLE `Escuela` (
  `IdEscuela` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Coordenadas` varchar(100) NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `FechaRegistro` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Muebles`
--

CREATE TABLE `Muebles` (
  `IdMueble` int(3) NOT NULL,
  `NombreMueble` varchar(25) NOT NULL,
  `Descripcion` varchar(50) NOT NULL,
  `Activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UsuarioAnfitrion`
--

CREATE TABLE `UsuarioAnfitrion` (
  `IdUsuarioAnfitrion` int(10) NOT NULL,
  `Correo` varchar(30) NOT NULL,
  `Contrasenia` varchar(15) NOT NULL,
  `Nombre` varchar(40) NOT NULL,
  `Apellido` varchar(50) NOT NULL,
  `Sexo` char(1) NOT NULL,
  `FechaNac` date NOT NULL,
  `Direccion` varchar(60) NOT NULL,
  `Ciudad` varchar(30) NOT NULL,
  `Estado` varchar(20) NOT NULL,
  `Pais` varchar(15) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `Calificacion` decimal(1,1) DEFAULT NULL,
  `Foto` varchar(50) DEFAULT NULL,
  `Activo` tinyint(1) NOT NULL,
  `FechaRegistro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UsuarioHuesped`
--

CREATE TABLE `UsuarioHuesped` (
  `IdUsuarioHuesped` int(10) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Contrasenia` varchar(15) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `Sexo` char(1) DEFAULT NULL,
  `Ciudad` varchar(30) NOT NULL,
  `Estado` varchar(30) NOT NULL,
  `Pais` varchar(20) NOT NULL,
  `Idioma` varchar(20) NOT NULL,
  `FechaNac` date NOT NULL,
  `Calificacion` decimal(1,1) DEFAULT NULL,
  `Foto` varchar(50) NOT NULL,
  `Activo` tinyint(1) NOT NULL,
  `FechaRegistro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Alojamiento`
--
ALTER TABLE `Alojamiento`
  ADD PRIMARY KEY (`IdAlojamiento`);

--
-- Indices de la tabla `Contratos`
--
ALTER TABLE `Contratos`
  ADD PRIMARY KEY (`IdContrato`);

--
-- Indices de la tabla `Det_Aloj_Muebles`
--
ALTER TABLE `Det_Aloj_Muebles`
  ADD PRIMARY KEY (`IdDetalle`);

--
-- Indices de la tabla `Escuela`
--
ALTER TABLE `Escuela`
  ADD PRIMARY KEY (`IdEscuela`);

--
-- Indices de la tabla `Muebles`
--
ALTER TABLE `Muebles`
  ADD PRIMARY KEY (`IdMueble`);

--
-- Indices de la tabla `UsuarioAnfitrion`
--
ALTER TABLE `UsuarioAnfitrion`
  ADD PRIMARY KEY (`IdUsuarioAnfitrion`);

--
-- Indices de la tabla `UsuarioHuesped`
--
ALTER TABLE `UsuarioHuesped`
  ADD PRIMARY KEY (`IdUsuarioHuesped`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Alojamiento`
--
ALTER TABLE `Alojamiento`
  MODIFY `IdAlojamiento` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Contratos`
--
ALTER TABLE `Contratos`
  MODIFY `IdContrato` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Det_Aloj_Muebles`
--
ALTER TABLE `Det_Aloj_Muebles`
  MODIFY `IdDetalle` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Escuela`
--
ALTER TABLE `Escuela`
  MODIFY `IdEscuela` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Muebles`
--
ALTER TABLE `Muebles`
  MODIFY `IdMueble` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `UsuarioAnfitrion`
--
ALTER TABLE `UsuarioAnfitrion`
  MODIFY `IdUsuarioAnfitrion` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `UsuarioHuesped`
--
ALTER TABLE `UsuarioHuesped`
  MODIFY `IdUsuarioHuesped` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
