<?php
	//header('Content-type: json/application');
	//header('Access-Control-Allow-Origin: *');

	ini_set('display_errors',0);
	error_reporting(E_ALL);

	$response = array();

	if(isset($_GET['action']))
	{
		require_once '../models/Users.php';
		$class = new Users();

		switch($_GET['action'])
		{
			case 'login_huesped': // Controlador de login Huesped
			{
				$response = $class->login_huesped($_POST['sesion_huesped']);
				//$response = $class->login_huesped();
				break;
			}
			case 'logout_huesped': // LOGOUT PENDIENTE
			{
				$response = $class->logout();
				break;
			}
			case 'login_anfitrion': // Controlador de login anfitrio
			{
				$response = $class->login_anfitrion($_POST['sesion_anfitrion']);
				break;
			}
			case 'logout_anfitrion': // Controlador de inserciones de la entidad de Titulos electrónicos
			{
				$response = $class->logout();
				break;
			}
			
			default:
				header('HTTP/1.1 400 Bad Request');
				$response = array(
					'status' => 'err',
					'body' => 'El objeto "'.$_GET['controller'].'" no existe'
				);
			
		}
		echo json_encode($response);
	}
	else
	{
		header('HTTP/1.1 400 Bad Request');
		$response = array(
			'status' => 'err',
			'body' => 'No se ha definido el objeto controller'
		);
		echo json_encode($response);
	}

 ?>
