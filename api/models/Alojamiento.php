<?php
  class Alojamiento{
    private $db;
    private $result = array(
      "status" => "",
      "body" => ""
    );
    public function __construct(){
      require_once 'ConnectDB.php';
      $class = new Connection();

      $this->db = $class->conectar();

    }

    public function get_alojamientoEspecifico($IdAlojamiento) // Obtiene un alojamiento especifico
    {
      try
      {
        $sql = $this->db->prepare("SELECT * FROM Alojamiento WHERE IdAlojamiento = :IdAlojamiento");
        $sql->bindParam(":IdAlojamiento", $IdAlojamiento, PDO::PARAM_INT);

        $sql->execute();

        if($sql->rowCount() > 0)
        {
          $this->result["status"] = "ok";
          $this->result["body"] = $sql->fetch(PDO::FETCH_ASSOC);
        }
        else
        {
          $this->result["status"] = "empty";
          $this->result["body"] = "No se encontro el alojamiento especificado. ID_SOLICITADO[".$id."]";
        }
      }
      catch(PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Error inesperado al intentar obtener el alojamiento. ID_SOLICITADO[".$id."] => ".$e->getMessage();
      }
      return $this->result;
    }

    public function get_alojamientos() // Obtiene el listado de Alojamientos
    {
      try
      {
        $sql = $this->db->prepare( "SELECT * FROM Alojamiento");

        $sql->execute();

        if($sql->rowCount() > 0)
        {
          $this->result["status"] = "ok";
          $this->result["body"] = $sql->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
          $this->result["status"] = "empty";
          $this->result["body"] = "No hay alojamientos disponibles";
        }
      }
      catch(PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Error inesperado al intentar obtener los alojamientos disponibles. => ".$e->getMessage();
      }
      return $this->result;
    }

    public function get_alojamientos_anfitrion($IdAnfitrion) // Obtiene el listado de Alojamientos
    {
      try
      {
        $sql = $this->db->prepare( "SELECT * FROM Alojamiento WHERE Anfitrion=:IdAnfitrion");
        $sql->bindParam(":IdAnfitrion", $IdAnfitrion, PDO::PARAM_INT);

        $sql->execute();

        if($sql->rowCount() > 0)
        {
          $this->result["status"] = "ok";
          $this->result["body"] = $sql->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
          $this->result["status"] = "empty";
          $this->result["body"] = "No hay alojamientos disponibles";
        }
      }
      catch(PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Error inesperado al intentar obtener los alojamientos disponibles. => ".$e->getMessage();
      }
      return $this->result;
    }

    public function insert_alojamiento($datosAlojamiento)
    {
      try
      {

        $sql = $this->db->prepare("INSERT INTO Alojamiento VALUES(null,
                                                                :NombreCorto,
                                                                :Direccion,
                                                                :Colonia,
                                                                :Municipio,
                                                                :Estado,
                                                                :CodigoPostal,
                                                                :Descripcion,
                                                                :NumPisos,
                                                                :NumCuartosCompletos,
                                                                :NumCuartosCompartidos,
                                                                :NumBaniosCompletos,
                                                                :NumBaniosMedios,
                                                                :NumCocinas,
                                                                :NumLavado,
                                                                :NumComedor,
                                                                :NumSalaDeEstar,
                                                                :NumPatios,
                                                                :NumAutosCochera,
                                                                :ServicioAgua,
                                                                :ServicioDrenaje,
                                                                :ServicioElectricidad,
                                                                :ServicioGas,
                                                                :ServicioCalentador,
                                                                :ServicioTelefono,
                                                                :ServicioInternet,
                                                                :ServicioTvCable,
                                                                :ServicioLimpieza,
                                                                1,
                                                                CURDATE(),
                                                                :ModalidadRenta,
                                                                :Huespedes,
                                                                :EstanciaMinima,
                                                                :CostoRenta,
                                                                :Foto,
                                                                :Anfitrion);");
        $sql->bindParam(":NombreCorto", $datosAlojamiento['NombreCorto'], PDO::PARAM_STR);                                                  
        $sql->bindParam(":Direccion", $datosAlojamiento['Direccion'], PDO::PARAM_STR);
        $sql->bindParam(":Colonia", $datosAlojamiento['Colonia'], PDO::PARAM_STR);
        $sql->bindParam(":Municipio", $datosAlojamiento['Municipio'], PDO::PARAM_STR);
        $sql->bindParam(":Estado", $datosAlojamiento['Estado'], PDO::PARAM_STR);
        $sql->bindParam(":CodigoPostal", $datosAlojamiento['CodigoPostal'], PDO::PARAM_STR);
        $sql->bindParam(":Descripcion", $datosAlojamiento['Descripcion'], PDO::PARAM_STR);
        $sql->bindParam(":NumPisos", $datosAlojamiento['NumPisos'], PDO::PARAM_INT);
        $sql->bindParam(":NumCuartosCompletos", $datosAlojamiento['NumCuartosCompletos'], PDO::PARAM_INT);
        $sql->bindParam(":NumCuartosCompartidos", $datosAlojamiento['NumCuartosCompartidos'], PDO::PARAM_INT);
        $sql->bindParam(":NumBaniosCompletos", $datosAlojamiento['NumBaniosCompletos'], PDO::PARAM_INT);
        $sql->bindParam(":NumBaniosMedios", $datosAlojamiento['NumBaniosMedios'], PDO::PARAM_STR);
        $sql->bindParam(":NumCocinas", $datosAlojamiento['NumCocinas'], PDO::PARAM_STR);
        $sql->bindParam(":NumLavado", $datosAlojamiento['NumLavado'] , PDO::PARAM_INT);
        $sql->bindParam(":NumComedor", $datosAlojamiento['NumComedor'] , PDO::PARAM_INT);
        $sql->bindParam(":NumSalaDeEstar", $datosAlojamiento['NumSalaDeEstar'] , PDO::PARAM_INT);
        $sql->bindParam(":NumPatios", $datosAlojamiento['NumPatios'] , PDO::PARAM_INT);
        $sql->bindParam(":NumAutosCochera", $datosAlojamiento['NumAutosCochera'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioAgua", $datosAlojamiento['ServicioAgua'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioDrenaje", $datosAlojamiento['ServicioDrenaje'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioElectricidad", $datosAlojamiento['ServicioElectricidad'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioGas", $datosAlojamiento['ServicioGas'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioCalentador", $datosAlojamiento['ServicioCalentador'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioTelefono", $datosAlojamiento['ServicioTelefono'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioInternet", $datosAlojamiento['ServicioInternet'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioTvCable", $datosAlojamiento['ServicioTvCable'] , PDO::PARAM_INT);
        $sql->bindParam(":ServicioLimpieza", $datosAlojamiento['ServicioLimpieza'] , PDO::PARAM_INT);
        $sql->bindParam(":ModalidadRenta", $datosAlojamiento['ModalidadRenta'] , PDO::PARAM_INT);
        $sql->bindParam(":Huespedes", $datosAlojamiento['Huespedes'] , PDO::PARAM_INT);      
        $sql->bindParam(":EstanciaMinima", $datosAlojamiento['EstanciaMinima'] , PDO::PARAM_INT);
        $sql->bindParam(":Anfitrion", $datosAlojamiento['Anfitrion'] , PDO::PARAM_INT);
        $sql->bindParam(":CostoRenta", $datosAlojamiento['CostoRenta'] , PDO::PARAM_INT);
        $sql->bindParam(":Foto", $datosAlojamiento['Foto'] , PDO::PARAM_STR);

        $sql->execute();
        $this->result["status"] = "ok";
        $this->result["body"] = "Alojamiento registrado con exito.";
      }
      catch (PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar registrar los datos del alojamiento";
      }
      return $this->result;
    }
    public function update_program($data)
    {
      try
      {
        $sql = $this->db->prepare(" UPDATE programas
                                    SET cve_programa = :cve_programa,
                                        nombre = :nombre,
                                        grupo = :grupo,
                                        modalidad = :mod,
                                        num_expediente = :num_exp,
                                        num_acuerdo = :num_ac,
                                        fecha_acuerdo = :fecha_ac,
                                        acentuacion = :acentuacion,
                                        num_periodo_oficial = :num_perOficial,
                                        fecha_periodo_oficial = :fecha_perOficial,
                                        fecha_ultModif = NOW(),
                                        activo = :activo
                                    WHERE id_usuario = :IdUsuario");

        $sql->bindParam(":cve_programa", $data['cve_programa'], PDO::PARAM_STR);
        $sql->bindParam(":nombre", $data['nombre'], PDO::PARAM_STR);
        $sql->bindParam(":grupo", $data['grupo'], PDO::PARAM_INT);
        $sql->bindParam(":mod", $data['modalidad'], PDO::PARAM_INT);
        $sql->bindParam(":num_exp", $data['num_expediente'], PDO::PARAM_STR);
        $sql->bindParam(":num_ac", $data['num_acuerdo'], PDO::PARAM_STR);
        $sql->bindParam(":fecha_ac", $data['fecha_acuerdo'], PDO::PARAM_STR);
        $sql->bindParam(":acentuacion", $data['acentuacion'], PDO::PARAM_INT);
        $sql->bindParam(":num_perOficial", $data['num_perOficial'], PDO::PARAM_STR);
        $sql->bindParam(":fecha_perOficial", $data['fecha_perOficial'], PDO::PARAM_STR);
        $sql->bindParam(":activo", $data['activo'] , PDO::PARAM_INT);
        //$sql->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);
        $sql->bindParam(":id", $_GET['id'], PDO::PARAM_INT);

        $sql->execute();

        $this->result["status"] = "ok";
        $this->result["body"] = "El programa de estudio con ID: ".$_GET['id']." ha sido actualizado con éxito.";

        $this->log->insert_activity_log('ACTUALIZAR', 'Programa de estudio ID['.$_GET['id'].']', $_SESSION['user_stamp']);
      }
      catch (PDOException $e)
      {
        //log de error
        $id_err = $this->log->insert_error_log(
          $e->getMessage(),
          $e->getFile(),
          $e->getLine(),
          $_SESSION['user_stamp']
        );
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar actualizar los datos del programa de estudio. ID del error: ".$id_err;
      }
      return $this->result;
    }

    public function delete_program($id) // Elimina de forma lógica el programa
    {
      try
      {
        $sql = $this->db->prepare("UPDATE programas SET activo = 0 WHERE id = :id AND id_usuario = :user_id");
        $sql->bindParam(":id", $id, PDO::PARAM_INT);
        $sql->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);

        $sql->execute();
        $this->result["status"] = "ok";
        $this->result["body"] = "El registro ha sido eliminado con éxito.";

        $this->log->insert_activity_log('ELIMINAR', 'Programa de estudio. ID: '.$id, $_SESSION['user_stamp']);
      }
      catch (PDOException $e)
      {
        //log del error
        //log del error
        $id_err = $this->log->insert_error_log(
          $e->getMessage(),
          $e->getFile(),
          $e->getLine(),
          $_SESSION['user_stamp']
        );
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar eliminar el registro. ID del error: ".$id_err;
      }
      return $this->result;
    }

  }
 ?>
