<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
  class Anfitrion{
    private $db;
    private $result = array(
      "status" => "",
      "body" => ""
    );
    public function __construct(){
      require_once 'ConnectDB.php';
      $class = new Connection();

      $this->db = $class->conectar();

    }

    public function get_anfitrion($IdAnfitrion) // Obtiene un anfitrion especifico
    {
      try
      {
        $sql = $this->db->prepare("SELECT * FROM UsuarioAnfitrion WHERE IdUsuarioAnfitrion = :IdAnfitrion");
        $sql->bindParam(":IdAnfitrion", $IdAnfitrion, PDO::PARAM_INT);

        $sql->execute();

        if($sql->rowCount() > 0)
        {
          $this->result["status"] = "ok";
          $this->result["body"] = $sql->fetch(PDO::FETCH_ASSOC);
        }
        else
        {
          $this->result["status"] = "empty";
          $this->result["body"] = "No se encontro el anfitrion especificado. ID_SOLICITADO[".$id."]";
        }
      }
      catch(PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Error inesperado al intentar obtener el anfitrion. ID_SOLICITADO[".$id."] => ".$e->getMessage();
      }
      return $this->result;
    }

    public function get_anfitriones() // Obtiene el listado de anfitriones
    {
      try
      {
        $sql = $this->db->prepare( "SELECT * FROM UsuarioAnfitrion");

        $sql->execute();

        if($sql->rowCount() > 0)
        {
          $this->result["status"] = "ok";
          $this->result["body"] = $sql->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
          $this->result["status"] = "empty";
          $this->result["body"] = "No hay anfitriones disponibles";
        }
      }
      catch(PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Error inesperado al intentar obtener los anfitriones disponibles. => ".$e->getMessage();
      }
      return $this->result;
    }

    public function insert_anfitrion($datos_anfitrion)
    {
      try
      {
        $sql = $this->db->prepare("INSERT INTO UsuarioAnfitrion VALUES(null,
                                                                :Correo,
                                                                :Contrasenia,
                                                                :Nombre,
                                                                :Apellido,
                                                                :Sexo,
                                                                :Idioma,
                                                                :FechaNac,
                                                                :Direccion,
                                                                :Ciudad,
                                                                :Estado,
                                                                :Telefono,
                                                                :Foto,
                                                                1,
                                                                CURDATE())");
        $sql->bindParam(":Correo", $datos_anfitrion['Correo'], PDO::PARAM_STR);
        $sql->bindParam(":Contrasenia", $datos_anfitrion['Contrasenia'], PDO::PARAM_STR);
        $sql->bindParam(":Nombre", $datos_anfitrion['Nombre'], PDO::PARAM_STR);
        $sql->bindParam(":Apellido", $datos_anfitrion['Apellido'], PDO::PARAM_STR);
        $sql->bindParam(":Sexo", $datos_anfitrion['Sexo'], PDO::PARAM_STR);
        $sql->bindParam(":Direccion", $datos_anfitrion['Direccion'], PDO::PARAM_STR);
        $sql->bindParam(":Idioma", $datos_anfitrion['Idioma'], PDO::PARAM_STR);
        $sql->bindParam(":FechaNac", $datos_anfitrion['FechaNac'], PDO::PARAM_STR);
        $sql->bindParam(":Ciudad", $datos_anfitrion['Ciudad'], PDO::PARAM_STR);
        $sql->bindParam(":Estado", $datos_anfitrion['Estado'], PDO::PARAM_STR);
        $sql->bindParam(":Telefono", $datos_anfitrion['Telefono'], PDO::PARAM_STR);
        $sql->bindParam(":Foto", $datos_anfitrion['Foto'], PDO::PARAM_STR);
        $sql->execute();
        $this->result["status"] = "ok";
        $this->result["body"] = "Anfitrion registrado con exito.";
      }
      catch (PDOException $e)
      {  
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar registrar el Usuario".$e->getMessage();;
      }
      return $this->result;
    }

    public function update_anfitrion($data)
    {
      try
      {
        $sql = $this->db->prepare(" UPDATE programas
                                    SET Correo = :Correo,
                                        Contrasenia = :Contrasenia,
                                        Nombre = :Nombre,
                                        Apellido = :Apellido,
                                        Sexo = :Sexo,
                                        Ciudad = :Ciudad,
                                        Estado = :Estado,
                                        Nacionalidad = :Nacionalidad,
                                        Idioma = :Idioma,
                                        FechaNac = :FechaNac,
                                        Foto = :Foto,
                                        Activo = :Activo,
                                        FechaRegistro = CURDATE()
                                    WHERE IdAnfitrion = :IdAnfitrion");
        $sql->bindParam(":Correo", $datos_anfitrion['Correo'], PDO::PARAM_STR);
        $sql->bindParam(":Contrasenia", $datos_anfitrion['Contrasenia'], PDO::PARAM_STR);
        $sql->bindParam(":Nombre", $datos_anfitrion['Nombre'], PDO::PARAM_STR);
        $sql->bindParam(":Apellido", $datos_anfitrion['Apellido'], PDO::PARAM_STR);
        $sql->bindParam(":Sexo", $datos_anfitrion['Sexo'], PDO::PARAM_STR);
        $sql->bindParam(":Direccion", $datos_anfitrion['Direccion'], PDO::PARAM_STR);
        $sql->bindParam(":Idioma", $datos_anfitrion['Idioma'], PDO::PARAM_STR);
        $sql->bindParam(":FechaNac", $datos_anfitrion['FechaNac'], PDO::PARAM_STR);
        $sql->bindParam(":Ciudad", $datos_anfitrion['Ciudad'], PDO::PARAM_STR);
        $sql->bindParam(":Estado", $datos_anfitrion['Estado'], PDO::PARAM_STR);
        $sql->bindParam(":Telefono", $datos_anfitrion['Telefono'], PDO::PARAM_STR);
        //$sql->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);
        $sql->bindParam(":id", $_GET['id'], PDO::PARAM_INT);

        $sql->execute();

        $this->result["status"] = "ok";
        $this->result["body"] = "USUARIO REGISTRADO CON EXITO";

        }
      catch (PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar actualizar los datos del programa de estudio. ID del error: ".$id_err;
      }
      return $this->result;
    }
    public function borrar_anfitrion($id) // Elimina de forma lógica el anfitrion
    {
      try
      {
        $sql = $this->db->prepare("UPDATE UsuarioAnfitrion SET Activo = 0 WHERE IdUsuarioAnfitrion = :id");
        $sql->bindParam(":id", $id, PDO::PARAM_INT);

        $sql->execute();
        $this->result["status"] = "ok";
        $this->result["body"] = "El usuario ha sido eliminado con éxito.";

        //$this->log->insert_activity_log('ELIMINAR', 'Programa de estudio. ID: '.$id, $_SESSION['user_stamp']);
      }
      catch (PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar eliminar el registro. ID del error: ".$id_err;
      }
      return $this->result;
    }
}
?>
