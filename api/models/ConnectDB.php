<?php
	//error_reporting(0);
	class Connection{

		public function conectar(){
			try{

				$conn = new PDO('mysql:host=localhost;dbname=movein_db','root','');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$conn->exec("SET CHARACTER SET UTF8");
			}catch(PDOException $e){
				die('Error: '.$e->getMessage());
				echo "Error en la linea: ".$e->getLine();
			}
			return $conn;

		}
	}

 ?>
