<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
  class Huesped{
    private $db;
    private $result = array(
      "status" => "",
      "body" => ""
    );
    public function __construct(){
      require_once 'ConnectDB.php';
      $class = new Connection();

      $this->db = $class->conectar();

    }

    public function get_huesped($id) // Obtiene un huesped especifico
    {
      try
      {
        $sql = $this->db->prepare("SELECT * FROM UsuarioHuesped WHERE IdUsuarioHuesped = :id");
        $sql->bindParam(":id", $id, PDO::PARAM_INT);

        $sql->execute();

        if($sql->rowCount() > 0)
        {
          $this->result["status"] = "ok";
          $this->result["body"] = $sql->fetch(PDO::FETCH_ASSOC);
        }
        else
        {
          $this->result["status"] = "empty";
          $this->result["body"] = "No se encontro el alojamiento especificado. ID_SOLICITADO[".$id."]";
        }
      }
      catch(PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Error inesperado al intentar obtener el alojamiento. ID_SOLICITADO[".$id."] => ".$e->getMessage();
      }
      return $this->result;
    }

    public function get_huespedes() // Obtiene el listado de huespedes
    {
      try
      {
        $sql = $this->db->prepare( "SELECT * FROM UsuarioHuesped");

        $sql->execute();

        if($sql->rowCount() > 0)
        {
          $this->result["status"] = "ok";
          $this->result["body"] = $sql->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
          $this->result["status"] = "empty";
          $this->result["body"] = "No hay alojamientos disponibles";
        }
      }
      catch(PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Error inesperado al intentar obtener los alojamientos disponibles. => ".$e->getMessage();
      }
      return $this->result;
    }

    public function insert_huesped($datos_huesped)
    {
      try
      {
        $sql = $this->db->prepare("INSERT INTO UsuarioHuesped VALUES(null,
                                                                :Correo,
                                                                :Contrasenia,
                                                                :Nombre,
                                                                :Apellido,
                                                                :Sexo,
                                                                :Nacionalidad,
                                                                :Idioma,
                                                                :FechaNac,
                                                                :Foto,
                                                                1,
                                                                CURDATE())");
        $sql->bindParam(":Correo", $datos_huesped['Correo'], PDO::PARAM_STR);
        $sql->bindParam(":Contrasenia", $datos_huesped['Contrasenia'], PDO::PARAM_STR);
        $sql->bindParam(":Nombre", $datos_huesped['Nombre'], PDO::PARAM_STR);
        $sql->bindParam(":Apellido", $datos_huesped['Apellido'], PDO::PARAM_STR);
        $sql->bindParam(":Sexo", $datos_huesped['Sexo'], PDO::PARAM_STR);
        $sql->bindParam(":Nacionalidad", $datos_huesped['Nacionalidad'], PDO::PARAM_STR);
        $sql->bindParam(":Idioma", $datos_huesped['Idioma'], PDO::PARAM_STR);
        $sql->bindParam(":FechaNac", $datos_huesped['FechaNac'], PDO::PARAM_STR);
        //$sql->bindParam(":Calificacion", $data['Calificacion'], PDO::PARAM_STR);
        $sql->bindParam(":Foto", $datos_huesped['Foto'], PDO::PARAM_STR);
        //$sql->bindParam(":Activo", $data['Activo'], PDO::PARAM_STR);


        $sql->execute();
        $this->result["status"] = "ok";
        $this->result["body"] = "HUESPED registrado con exito.";
      }
      catch (PDOException $e)
      {  
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar registrar el Usuario".$e->getMessage();;
      }
      return $this->result;
    }

    public function update_huesped($data)
    {
      try
      {
        $sql = $this->db->prepare(" UPDATE programas
                                    SET Correo = :Correo,
                                        Contrasenia = :Contrasenia,
                                        Nombre = :Nombre,
                                        Apellido = :Apellido,
                                        Sexo = :Sexo,
                                        Ciudad = :Ciudad,
                                        Estado = :Estado,
                                        Nacionalidad = :Nacionalidad,
                                        Idioma = :Idioma,
                                        FechaNac = :FechaNac,
                                        Foto = null,
                                        Activo = :Activo,
                                        FechaRegistro = CURDATE()
                                    WHERE IdHuesped = :IdHuesped");
        //DUDA De donde se obtiene el id de la sesion
        $sql->bindParam(":Correo", $data['Correo'], PDO::PARAM_STR);
        $sql->bindParam(":Contrasenia", $data['Contrasenia'], PDO::PARAM_STR);
        $sql->bindParam(":Nombre", $data['Nombre'], PDO::PARAM_INT);
        $sql->bindParam(":Apellido", $data['Apellido'], PDO::PARAM_STR);
        $sql->bindParam(":Sexo", $data['Sexo'], PDO::PARAM_STR);
        $sql->bindParam(":Ciudad", $data['Ciudad'], PDO::PARAM_STR);
        $sql->bindParam(":Estado", $data['Estado'], PDO::PARAM_STR);
        $sql->bindParam(":Nacionalidad", $data['Nacionalidad'], PDO::PARAM_INT);
        $sql->bindParam(":Idioma", $data['Idioma'], PDO::PARAM_STR);
        $sql->bindParam(":FechaNac", $data['FechaNac'], PDO::PARAM_STR);
        $sql->bindParam(":Activo", $data['Activo'] , PDO::PARAM_INT);
        //$sql->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);
        $sql->bindParam(":IdHuesped", $_GET['IdHuesped'], PDO::PARAM_INT);

        $sql->execute();

        $this->result["status"] = "ok";
        $this->result["body"] = "El usuario ".$_GET['IdHuesped']." ha sido actualizado con éxito.";
}
      catch (PDOException $e)
      {
        
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar actualizar los datos ";
      }
      return $this->result;
    }

    public function borrar_huesped($id) // Elimina de forma lógica el huesped
    {
      try
      {
        $sql = $this->db->prepare("UPDATE UsuarioHuesped SET Activo = 0 WHERE id = :id AND id_usuario = :user_id");
        $sql->bindParam(":id", $id, PDO::PARAM_INT);
        $sql->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);

        $sql->execute();
        $this->result["status"] = "ok";
        $this->result["body"] = "El usuario ha sido eliminado con éxito.";

        //$this->log->insert_activity_log('ELIMINAR', 'Programa de estudio. ID: '.$id, $_SESSION['user_stamp']);
      }
      catch (PDOException $e)
      {
        $this->result["status"] = "err";
        $this->result["body"] = "Un error ha ocurrido al intentar eliminar el registro";
      }
      return $this->result;
    }
  }
 ?>
