<?php
    class Users{
        private $db;
        private $result = array(
        "status" => "",
        "body" => ""
        );
        public function __construct(){
            require_once 'ConnectDB.php';
            $class = new Connection();

            $this->db = $class->conectar();
            
        } 
        
        function login_huesped($sesion_huesped){
            try{
                $sql = $this->db->prepare("SELECT IdUsuarioHuesped, Correo, Contrasenia FROM UsuarioHuesped WHERE Correo = :Correo AND Contrasenia = :Contrasenia");
                $sql->bindParam(":Correo", $sesion_huesped['Correo'], PDO::PARAM_STR);
                $sql->bindParam(":Contrasenia", $sesion_huesped['Contrasenia'], PDO::PARAM_STR);
                
                $sql->execute();

                if($sql->rowCount() > 0)
                {
                    $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
					
                    
                    
                    $this->result["status"] = "ok";
                    $this->result["body"] = $sql->fetch(PDO::FETCH_ASSOC);
                    return $this->result;
                }
                else
                {
                    $this->result["status"] = "empty";
                    $this->result["body"] = "No se encontró usuario registrado con esos datos";
                }
            }
            catch(PDOException $e){
                $this->result["status"] = "err";
                $this->result["body"] = "Error inesperado al intentar iniciar sesión. => ".$e->getMessage();    
            }
            return $this->result;
        }

        function login_anfitrion($sesion_anfitrion){
            try{
                $sql = $this->db->prepare("SELECT IdUsuarioAnfitrion, Correo, Contrasenia FROM UsuarioAnfitrion WHERE Correo = :Correo AND Contrasenia = :Contrasenia");
                $sql->bindParam(":Correo", $sesion_anfitrion['Correo'], PDO::PARAM_STR);
                $sql->bindParam(":Contrasenia", $sesion_anfitrion['Contrasenia'], PDO::PARAM_STR);
                
                $sql->execute();

                if($sql->rowCount() > 0)
                {
                    $this->result["status"] = "ok";
                    $this->result["body"] = $sql->fetch(PDO::FETCH_ASSOC);
                    //$DatosAnfitrion = $sql->fetch(PDO::FETCH_ASSOC);


                    
                    $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
					session_start();
					$_SESSION['Correo'] = $this->result["body"]['Correo'];
					$_SESSION['IdAnfitrion'] =  $this->result["body"]['IdUsuarioAnfitrion'];
                    
                    
                    return $this->result;
                }
                else
                {
                    $this->result["status"] = "empty";
                    $this->result["body"] = "No se encontró usuario registrado con esos datos";
                }
            }
            catch(PDOException $e){
                $this->result["status"] = "err";
                $this->result["body"] = "Error inesperado al intentar iniciar sesión. => ".$e->getMessage();    
            }
            return $this->result;
        }
    }
?>