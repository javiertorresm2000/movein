<?php
  header('Access-Control-Allow-Origin: *');//Permitir peticiones externas (para las peticiones ajax del front)
  ini_set('display_errors', 1);//Configuración para desplegar los errores. 0 Inactiva y 1 Activa
  error_reporting(E_ALL);//Retorna errores de cualquier nivel, WARNINGS y ERRORS incluidos
  //Incluyo el archivo Alojamiento.php para leer las respuestas de la base de datos
  require_once '../models/Alojamiento.php';
  $class = new Alojamiento(); // Creo un objeto de la clase Alojamiento

  $response = array();

  //Leo el método (verbo http) con el que se está solicitando
  switch($_GET['action'])
  {
    case 'select_alojamiento': //Obtener registros 
    {
      
        $response = $class->get_alojamientos();
      break;
    }

    case 'select_alojamiento_especifico':
    {
      $response = $class->get_alojamientoEspecifico($_GET['IdAlojamiento']);
    break;
    }

    case 'select_alojamientos_anfitrion': //Obtener registros deL ANFITRION
    {
        $response = $class->get_alojamientos_anfitrion($_GET['IdAnfitrion']);
      break;
    }

    case 'insert_alojamiento': // Crear un nuevo alojamiento
    {
      $response = $class->insert_alojamiento($_POST['datosAlojamiento']);
      break;
    }

    case 'PUT': // Actualizo un alojamiento
    {
      $put_param = array();
      parse_str(file_get_contents('php://input'), $put_param);
  		$GLOBALS['_PUT'] = $put_param;

      break;
    }

    case 'DELETE': // Elimino un alojamiento
    {

      break;
    }

    default:
      header('HTTP/1.1 405 Not Found');
      header('Allow: GET, POST, PUT, DELETE');
      break;
  }
  echo json_encode($response);
 ?>
